#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <string>

#include <tp5.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;


std::vector<string> TP5::names(
{
	"Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
	"JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
	"Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
	"Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
	"Fanny", "Jeanne", "Elo"
});


//// Needed functions
int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
	return (int)(element.at(0))%this->size();
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
	this->get(this->hash(element)) = element;
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount){
	for (int i = 0; i < namesCount; i++){
		table.insert(names[i]);
	}
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
	if (this->get(this->hash(element)) == element){
		return true;
	}
    return false;
}
//// Needed functions





unsigned long int hash(string key){
	size_t h = 0;
	int factor = 128;
	// return an unique hash id from key
	for (int i = 0; i < key.size(); i++){
		h *= factor;
		h += (int)key[i];
	}
	return h;
}

struct MapNode : public BinaryTree
{

	string key;
	unsigned long int key_hash;

	int value;

	MapNode* left;
	MapNode* right;

	MapNode(string key, int value) : BinaryTree (value)
	{
		this->key = key;
		this->value = value;
		this->key_hash = hash(key);

		this->left = this->right = nullptr;
	}

	/**
	 * @brief insertNode insert a new node according to the key hash
	 * @param node
	 */
	void insertNode(MapNode* node){
		if (node->key_hash > this->key_hash){
			if (right == NULL){
				right = node;
			} else {
				right->insertNode(node);
			}
		} else {
			if (left == NULL){
				left = node;
			} else {
				left->insertNode(node);
			}
		}
	}

	void insertNode(string key, int value)
	{
		this->insertNode(new MapNode(key, value));
	}

	virtual ~MapNode() {}
	QString toString() const override {return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value);}
	Node* get_left_child() const {return left;}
	Node* get_right_child() const {return right;}
};

struct Map
{
	Map() {
		this->root = nullptr;
	}

	/**
	 * @brief insert create a node and insert it to the map
	 * @param key
	 * @param value
	 */
	void insert(string key, int value)
	{
		if (this->root == NULL){
			this->root = new MapNode(key, value);
		} else {
			this->root->insertNode(key, value);
		}
	}

	/**
	 * @brief get return the value of the node corresponding to key
	 * @param key
	 * @return
	 */
	int get(string key){
		MapNode* current = this->root;
		size_t h = hash(key);
		while (current != NULL && current->key_hash != h){
			if (current->key_hash < h){
				current = current->right;
			} else {
				current = current->left;
			}
		}
		if (current == NULL){
			return -1;
		}
		return current->value;
	}
	MapNode* root;
};


int main(int argc, char *argv[])
{
	srand(time(NULL));
	Map map;

	map.insert("Yolo", 20);
	for (std::string& name : TP5::names)
	{
		if (rand() % 3 == 0)
		{
			map.insert(name, rand() % 21);
		}
	}

	printf("map[\"Margot\"]=%d\n", map.get("Margot"));
	printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
	printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
	printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
	printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
	printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));


	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
	w = new MapWindow(*map.root);
	w->show();
	return a.exec();
}
