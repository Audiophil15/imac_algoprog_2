#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	int min;
	int indexMin = 0;
	int tmp;
	int i,j;
	for (i = 0; i < toSort.size(); i++){
		min = toSort[i];
		indexMin = i;
		for (j = i; j < toSort.size(); j++){
			if (toSort[j]<min){
				min = toSort[j];
				indexMin = j;
			}
		}
		tmp = toSort[i];
		toSort[i] = toSort[indexMin];
		toSort[indexMin] = tmp;
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
