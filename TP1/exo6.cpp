#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
	Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
	int size;
	int nbVals;
	// int ajoute(int val){
	// 	if (nbVals==size){
	// 		size+=10;
	// 		int* newArea = (int*)realloc(donnees, sizeof(int)*size);
	// 		if (newArea==NULL){
	// 			return -1;
	// 		}
	// 		donnees = newArea;
	// 	}
	// 	donnees[nbVals]=val;
	// 	nbVals+=1;
	// 	return 0;
	// }

	// int recupere(int index){
	// 	return donnees[index];
	// }

	// int cherche(int value){
	// 	for (int i = 0; i < nbVals; i++){
	// 		if (donnees[i]==value){
	// 			return i;
	// 		}
	// 	}
	// 	return -1;
	// }

	// void stocke(int val, int index){
	// 	if (index<size){
	// 		donnees[index] = val;
	// 	}
	// }


    // your code
};


void initialise(Liste* liste){
	liste->premier = NULL;
	liste->dernier = NULL;
}

bool est_vide(const Liste* liste){
	if (liste->premier == NULL){
		return true;
	}
    return false;
}

void ajoute(Liste* liste, int valeur){
	if (liste->dernier!=NULL){
		liste->dernier->suivant = (Noeud*)malloc(sizeof(Noeud));
		liste->dernier = liste->dernier->suivant;
	} else {
		liste->premier = (Noeud*)malloc(sizeof(Noeud));
		liste->dernier = liste->premier;
	}
	liste->dernier->donnee = valeur;
	liste->dernier->suivant = NULL;
}

void affiche(const Liste* liste){
	Noeud* head = liste->premier;
	while (head!=NULL){
		printf("%d\n", head->donnee);
		head = head->suivant;
	}
}

int recupere(const Liste* liste, int n){
	Noeud* node = liste->premier;
	int i=0;
	while (node!=NULL && i<n){
		node = node->suivant;
		i++;
	}
	if (node!=NULL){
		return node->donnee;
	}	
    return 0;
}

int cherche(const Liste* liste, int valeur){
	Noeud* node = liste->premier;
	int i=0;
	while (node!=NULL){
		if (node->donnee == valeur){
			return i;
		}
		node = node->suivant;
		i++;
	}
    return -1;
}

void stocke(Liste* liste, int n, int valeur){
	Noeud* node = liste->premier;
	while (node != NULL && n>0){
		node = node->suivant;
	}
	if (node!=NULL){
		node->donnee = valeur;
	}
}

void ajoute(DynaTableau* tableau, int valeur){
 	if (tableau->nbVals==tableau->size){
		tableau->size+=10;
		int* newArea = (int*)realloc(tableau->donnees, sizeof(int)*tableau->size);
		if (newArea!=NULL){
			tableau->donnees = newArea;
		}
	}
	tableau->donnees[tableau->nbVals]=valeur;
	tableau->nbVals+=1;
}


void initialise(DynaTableau* tableau, int capacite){
	tableau->size = capacite;
	tableau->nbVals = 0;
	tableau->donnees = (int*)malloc(sizeof(int)*capacite);
}

bool est_vide(const DynaTableau* liste){
		return liste->nbVals==0;
}

void affiche(const DynaTableau* tableau){
	for (int i = 0; i < tableau->nbVals; i++){
		printf("%d\n", tableau->donnees[i]);
	}
}

int recupere(const DynaTableau* tableau, int n){
	if (n<tableau->nbVals && tableau->nbVals>0){
		return tableau->donnees[n];
	}
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur){
	for (int i = 0; i < tableau->nbVals; i++){
		if (tableau->donnees[i]==valeur){
			return i;
		}
	}
	
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur){
	if (n<tableau->nbVals && tableau->nbVals>0){
		tableau->donnees[n] = valeur;
	}
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur){
	ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste){
	if (liste->premier!=NULL){
		Noeud* head = liste->premier;
		int val = head->donnee;
		liste->premier = liste->premier->suivant;
		free(head);
		return val;
	}
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur){
	Noeud* node = (Noeud*)malloc(sizeof(Noeud));
	node->donnee = valeur;
	node->suivant = liste->premier;
	liste->premier = node;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste){
	if (liste->premier!=NULL){
		Noeud* head = liste->premier;
		int val = liste->premier->donnee;
		liste->premier = liste->premier->suivant;
		free(head);
		return val;
	}
    return 0;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
	std::cout << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;
	std::cout << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
	std::cout << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);


    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

	std::cout << std::endl;

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
