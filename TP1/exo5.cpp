#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot


	if (z.length()<2){
		return 1;
	}
	if (n>0){
		Point z1;
		z1.x = z.x*z.x-z.y*z.y+point.x;
		z1.y = 2*z.x*z.y+point.y;
		return isMandelbrot(z1, n-1, point);
	}
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}
