#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <list>
#include <string>

typedef struct Node{
	Node* left;
	Node* right;
	Node* parent;
	int value;

	void initNode(int value){
		this->left = NULL;
		this->right = NULL;
		this->parent = NULL;
		this->value = value;
		// init initial node without children
	}

	void insertNumber(int value) {
		int rebalance = 0;
		if (value < this->value){
			if (this->right!=NULL){
				this->right->insertNumber(value);
			} else {
				this->right = (Node*)malloc(sizeof(Node));
				this->right->value = value;
				this->right->right=NULL;
				this->right->left=NULL;
				this->right->parent = this;
				rebalance = 1;
			}
		} else {
			if (this->left != NULL){
				this->left->insertNumber(value);
			} else {
				this->left = (Node*)malloc(sizeof(Node));
				this->left->value = value;
				this->left->right=NULL;
				this->left->left=NULL;
				this->left->parent = this;
				rebalance = 1;
			}
		}
		if (rebalance){
			Node* node = this;
			uint bal = searchImbalancedNode(node);
			// printf("Add : %d | Unbal : %p, %d\n", value, node, node!=NULL ? node->value : -1);
			if (node != NULL){
				balanceTree(node, bal);
			}
		}
		// balanceTree(this);
		// create a new node and insert it in right or left child
	}

	uint height() const	{
		if (this->isLeaf()){
			return 1;
		}
		int hl=1;
		int hr=1;
		if (left != NULL){
			hl += left->height();
		}
		if (right != NULL){
			hr += right->height();
		}
		if (hr > hl){
			return hr;
		}
		return hl;		
		// should return the maximum height between left child and
		// right child +1 for itself. If there is no child, return
		// just 1
	}

	uint nodesCount() const {
		// should return the sum of nodes within left child and
		// right child +1 for itself. If there is no child, return
		// just 1

		if (left == NULL && right == NULL){
			return 1;
		}
		int cpt = 1;
		if (left){
			cpt += left->nodesCount();
		}
		if (right){
			cpt += right->nodesCount();
		}
		return cpt;

	}

	bool isLeaf() const {
		return (left==NULL&&right==NULL);
		// return True if the node is a leaf (it has no children)
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
		if (this->isLeaf()){
			leaves[leavesCount] = this;
			leavesCount += 1;
		}
		if (this->left!=NULL){
			this->left->allLeaves(leaves, leavesCount);
		}
		if (this->right!=NULL){
			this->right->allLeaves(leaves, leavesCount);
		}
		// fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
		if (this->left!=NULL){
			this->left->inorderTravel(nodes, nodesCount);
		}
		nodes[nodesCount] = this;
		nodesCount += 1;
		if (this->right!=NULL){
			this->right->inorderTravel(nodes, nodesCount);
		}
		// fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
		nodes[nodesCount] = this;
		nodesCount += 1;
		if (this->left!=NULL){
			this->left->preorderTravel(nodes, nodesCount);
		}
		if (this->right!=NULL){
			this->right->preorderTravel(nodes, nodesCount);
		}
		// fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
		if (this->left!=NULL){
			this->left->postorderTravel(nodes, nodesCount);
		}
		if (this->right!=NULL){
			this->right->postorderTravel(nodes, nodesCount);
		}
		nodes[nodesCount] = this;
		nodesCount += 1;
		// fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
		if (this->value == value){
			return this;
		} else {
			if (this->value < value ){
				if (this->right != NULL){
					return this->right->find(value);
				}
			} else if (this->left != NULL){
				return this->left->find(value);
			}
		}
		// find the node containing value
		return nullptr;
	}

	uint balance(){
		if (this->right==this->left){
			return 0;
		} else {
			if(this->right==NULL){
				return this->left->height();
			} else if(this->left==NULL){
				return this->right->height();
			}
		}
		return this->left->height()-this->right->height();
	}

	void rotateLeft(){
		Node* parent = this->parent;
		Node* tmp = this->right;
		tmp->parent = this->parent;
		if (parent!=NULL){
			if (this==parent->left){
				parent->left = this->right;
			} else {
				parent->right = this->right;
			}
		}
		if (this->right != NULL){
			this->right->parent = this;
		}
		this->right = this->right->left;
		tmp->left = this;
		this->parent = tmp;
	}

	void rotateRight(){
		Node* parent = this->parent;
		Node* tmp = this->left;
		tmp->parent = this->parent;
		if (parent!=NULL){
			if (this==parent->left){
				parent->left = this->left;
			} else {
				parent->right = this->left;
			}
		}
		this->left = this->left->right;
		if (this->left != NULL){
			this->left->parent = this;
		}
		tmp->right = this;
		this->parent = tmp;
	}

	void reset()
	{
		if (left != NULL)
			delete left;
		if (right != NULL)
			delete right;
		left = right = NULL;
	}

	uint searchImbalancedNode(Node*& node){
		uint bal = node->balance();
		while (node != NULL && abs((int)bal)<2){
			node = node->parent;
			if (node!=NULL){
				bal = node->balance();
			}
		}
		return bal;
	}

	void balanceTree(Node* node, uint bal){
		if (node!=NULL){
			// left is unbalanced
			if (bal==2){
				// 
				if (node->left!=NULL && node->left->balance()==-1){
					node->left->rotateLeft();
				}
				node->rotateRight();
			}
			// right is unbalanced (bal==-2)
			else if (bal==-2){
				if (node->right!=NULL && node->right->balance()==1){
					node->right->rotateRight();
				}
				node->rotateLeft();
			}
		}
	}

	int nodeDepth(){
		int d = 0;
		Node* current = this;
		while (current->parent != NULL){
			current = current->parent;
			d++;
		}
		return d;
	}

	Node* getRoot(){
		Node* current = this;
		while (current->parent != NULL){
			current = current->parent;
		}
		return current;
	}

	int get_value() const {return value;}
	Node* get_left_child() const {return left;}
	Node* get_right_child() const {return right;}
} Node;

typedef struct Noeud{
	Node* node;
	Noeud* next;
} Noeud;

typedef struct Liste{
	Noeud* first;
	Noeud* last;
	// your code
} Liste;

Node* retire_file(Liste* liste){
	if (liste->first!=NULL){
		Noeud* head = liste->first;
		Node* val = head->node;
		liste->first = liste->first->next;
		free(head);
		if (liste->first == NULL){
			liste->last = NULL;
		}
		return val;
	}
	return 0;
}

void initialise(Liste* liste){
	liste->first = NULL;
	liste->last = NULL;
}

bool est_vide(const Liste* liste){
	if (liste->first == NULL){
		return true;
	}
	return false;
}

void ajoute(Liste* liste, Node* valeur){
	if (liste->last!=NULL){
		liste->last->next = (Noeud*)malloc(sizeof(Noeud));
		liste->last = liste->last->next;
	} else {
		liste->first = (Noeud*)malloc(sizeof(Noeud));
		liste->last = liste->first;
	}
	liste->last->node = valeur;
	liste->last->next = NULL;
}

void affiche(const Liste* liste){
	Noeud* head = liste->first;
	while (head!=NULL){
		printf("%d\n", head->node);
		head = head->next;
	}
}

Node* recupere(const Liste* liste, int n){
	Noeud* node = liste->first;
	int i=0;
	while (node!=NULL && i<n){
		node = node->next;
		i++;
	}
	if (node!=NULL){
		return node->node;
	}	
	return 0;
}

void stocke(Liste* liste, int n, Node* valeur){
	Noeud* node = liste->first;
	while (node != NULL && n>0){
		node = node->next;
	}
	if (node!=NULL){
		node->node = valeur;
	}
}

void pousse_file(Liste* liste, Node* valeur){
	ajoute(liste, valeur);
}

void printTree(Node* root){
	Liste file;
	Node* tmp;
	int depth[25];
	for (int k=0; k<25;k++){
		depth[k]=0;
	}
	int d;

	int maxdepth = root->height();
	printf("md : %d\n", maxdepth);

	// Creer file
	initialise(&file);
	
	// ajoute la racine
	pousse_file(&file, root);

	while (!est_vide(&file)){
		tmp = retire_file(&file);
		d = tmp->nodeDepth();
		if (tmp!=NULL){
			if (depth[d]==0){
				for (int i = 0; i < 4*pow(2,(maxdepth-d))-4; i++){
					printf(" ");
				}
			}


			printf("%-4d", tmp->value);
			if (tmp->parent != NULL && tmp == tmp->parent->left){
				for (int i = 0; i < 8*pow(2,(maxdepth-d))-4; i++){
					printf("_");
				}
			} else {
				for (int i = 0; i < 8*pow(2,(maxdepth-d))-4; i++){
					printf(" ");
				}
			}
			
			if (tmp->left != NULL){
				pousse_file(&file, tmp->left);
			}

			if (tmp->right != NULL){
				pousse_file(&file, tmp->right);
			}
			depth[d]+=1;
		}

		if (depth[d]==pow(2, d)){
			printf("\n");
			for (int i = 0; i < pow(2,d); i++){
				for (int i = 0; i < 4*pow(2,(maxdepth-d))-2; i++){
					printf(" ");
				}
				printf("|");
				for (int i = 0; i < 4*pow(2,(maxdepth-d))+1; i++){
					printf(" ");
				}
			}
			printf("\n");
			
		}
	}
}

void depthPrint(Node* root, int fromdepth){
	// for (int i = 0; i < fromdepth; i++){
	// 	printf("     ");
	// }
	
	printf("%3d\n",root->value);
	if (root->left){
		depthPrint(root->left, fromdepth+1);
	}
	if (root->right){
		depthPrint(root->right, fromdepth+1);
	}
	if (root->isLeaf()){
		printf("\n");
	}
	
}

void insertNode(Node*& root, int value){
	root->insertNumber(value);
	if (root->parent!=NULL){
		root = root->parent;
	}
}

int main(){

	srand(time(NULL));

	Node head;
	Node* root = &head;
	root->initNode(0);

	for (int i = 1; i < 121; i++) {
		insertNode(root, i);
		// insertNode(root, rand()%128);
		printf("Tree : \n");
		printTree(root);
		printf("\n\n");
	}
	
	return 0;

}