#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{
    (SearchTreeNode)* left;
    SearchTreeNode* right;
	SearchTreeNode* parent;
    int value;

	void initNode(int value){
		this->left = NULL;
		this->right = NULL;
		this->parent = NULL;
		this->value = value;
		// init initial node without children
	}

	void insertNumber(int value) {
		int rebalance = 0;
		if (value < this->value){
			if (right!=NULL){
				right->insertNumber(value);
			} else {
				right = new SearchTreeNode(value);
				right->parent = this;
				rebalance = 1;
			}
		} else {
			if (left != NULL){
				left->insertNumber(value);
			} else {
				left = new SearchTreeNode(value);
				left->parent = this;
				rebalance = 1;
			}
		}
		if (rebalance){
			SearchTreeNode* node = this;
			uint bal = searchImbalancedNode(node);
			if (node != NULL){
				balanceTree(node, bal);
			}
		}
		// balanceTree(this);
		// create a new node and insert it in right or left child
	}

	uint height() const	{
		if (this->isLeaf()){
			return 1;
		}
		int hl=1;
		int hr=1;
		if (left != NULL){
			hl += left->height();
		}
		if (right != NULL){
			hr += right->height();
		}
		if (hr > hl){
			return hr;
		}
		return hl;
		// should return the maximum height between left child and
		// right child +1 for itself. If there is no child, return
		// just 1
	}

	uint nodesCount() const {
		// should return the sum of nodes within left child and
		// right child +1 for itself. If there is no child, return
		// just 1

		if (left == NULL && right == NULL){
			return 1;
		}
		int cpt = 1;
		if (left){
			cpt += left->nodesCount();
		}
		if (right){
			cpt += right->nodesCount();
		}
		return cpt;

	}

	bool isLeaf() const {
		return (left==NULL&&right==NULL);
		// return True if the node is a leaf (it has no children)
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
		if (this->isLeaf()){
			leaves[leavesCount] = this;
			leavesCount += 1;
		}
		if (this->left!=NULL){
			this->left->allLeaves(leaves, leavesCount);
		}
		if (this->right!=NULL){
			this->right->allLeaves(leaves, leavesCount);
		}
		// fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
		if (this->left!=NULL){
			this->left->inorderTravel(nodes, nodesCount);
		}
		nodes[nodesCount] = this;
		nodesCount += 1;
		if (this->right!=NULL){
			this->right->inorderTravel(nodes, nodesCount);
		}
		// fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
		nodes[nodesCount] = this;
		nodesCount += 1;
		if (this->left!=NULL){
			this->left->preorderTravel(nodes, nodesCount);
		}
		if (this->right!=NULL){
			this->right->preorderTravel(nodes, nodesCount);
		}
		// fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
		if (this->left!=NULL){
			this->left->postorderTravel(nodes, nodesCount);
		}
		if (this->right!=NULL){
			this->right->postorderTravel(nodes, nodesCount);
		}
		nodes[nodesCount] = this;
		nodesCount += 1;
		// fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
		if (this->value == value){
			return this;
		} else {
			if (this->value < value ){
				if (this->right != NULL){
					return this->right->find(value);
				}
			} else if (this->left != NULL){
				return this->left->find(value);
			}
		}
		// find the node containing value
		return nullptr;
	}

	uint balance(){
		if (this->right==this->left){
			return 0;
		} else {
			if(this->right==NULL){
				return this->left->height();
			} else if(this->left==NULL){
				return this->right->height();
			}
		}
		return this->left->height()-this->right->height();
	}

	void rotateLeft(){
		SearchTreeNode* parent = this->parent;
		SearchTreeNode* tmp = this->right;
		tmp->parent = this->parent;
		if (parent!=NULL){
			if (this==parent->left){
				parent->left = this->right;
			} else {
				parent->right = this->right;
			}
		}
		if (this->right != NULL){
			this->right->parent = this;
		}
		this->right = this->right->left;
		tmp->left = this;
		this->parent = tmp;
	}

	void rotateRight(){
		SearchTreeNode* parent = this->parent;
		SearchTreeNode* tmp = this->left;
		tmp->parent = this->parent;
		if (parent!=NULL){
			if (this==parent->left){
				parent->left = this->left;
			} else {
				parent->right = this->left;
			}
		}
		this->left = this->left->right;
		if (this->left != NULL){
			this->left->parent = this;
		}
		tmp->right = this;
		this->parent = tmp;
	}

	void reset()
	{
		if (left != NULL)
			delete left;
		if (right != NULL)
			delete right;
		left = right = NULL;
	}

	uint searchImbalancedNode(SearchTreeNode*& node){
		uint bal = node->balance();
		while (node != NULL && abs((int)bal)<2){
			node = node->parent;
			if (node!=NULL){
				bal = node->balance();
			}
		}
		return bal;
	}

	void balanceTree(SearchTreeNode* node, uint bal){
		if (node!=NULL){
			// left is unbalanced
			if (bal==2){
				//
				if (node->left!=NULL && node->left->balance()==-1){
					node->left->rotateLeft();
				}
				node->rotateRight();
			}
			// right is unbalanced (bal==-2)
			else if (bal==-2){
				if (node->right!=NULL && node->right->balance()==1){
					node->right->rotateRight();
				}
				node->rotateLeft();
			}
		}
	}

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};


Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
