#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex){
    return 2*nodeIndex+1;
}

int Heap::rightChild(int nodeIndex)
{
    return 2*nodeIndex+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;

	(*this)[i] = value;
	int tmp;

	while (i>0 && (*this)[i]>(*this)[(i-1)/2]){
		tmp = (*this)[i];
		(*this)[i] = (*this)[(i-1)/2];
		(*this)[(i-1)/2] = tmp;

		i = (i-1)/2;
	}

}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;

	int maxval = (*this)[nodeIndex];
	int maxindex = nodeIndex;
	int tmp;
	int left = leftChild(nodeIndex);
	int right = rightChild(nodeIndex);

	if (left<heapSize && maxval<(*this)[left]){
		maxval=(*this)[left];
		maxindex = left;
	}
	if (right<heapSize && maxval<(*this)[right]){
		maxval=(*this)[right];
		maxindex = right;
	}

	if (maxindex != i_max){
		tmp = (*this)[nodeIndex];
		(*this)[nodeIndex] = (*this)[maxindex];
		(*this)[maxindex] = tmp;
		this->heapify(heapSize, maxindex);
	}
}

void Heap::buildHeap(Array& numbers){
	int size = numbers.size();
	for (int i = 0; i < size; i++){
		this->insertHeapNode(size, numbers[i]);
	}
}

void Heap::heapSort(){
	for (int i = this->size()-1; i >= 0; i--){
		int tmp = (*this)[0];
		(*this)[0] = (*this)[i];
		(*this)[i] = tmp;
		heapify(this->size(), 0);
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
